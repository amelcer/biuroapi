const mongoose = require("mongoose");


const RoomsInfoSchema = mongoose.Schema({
    date: {
        type: Date,
        required: true,
    },
    leavingRooms: {
        type: [Number],
        default: [],
    },
    arrivingRooms: {
        type: [Number],
        default: [],
    },
});

module.exports = mongoose.model("RoomsInfo", RoomsInfoSchema);

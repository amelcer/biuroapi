const express = require("express");
const router = express.Router();
const RoomsInfo = require("../models/Rooms");

router.get("/getAllRooms", async (req, res) => {
    try {
        const response = await RoomsInfo.find();
        res.send({ response });
    } catch (error) {
        res.status(500).send( {  error } );
    }
});

router.get("/getRooms", async (req, res) => {
    const { from, to } = req.query;
    try {
        if (!from || !to)
            throw 'Too few arguments'
        const response = await RoomsInfo.find({ date: { $gte: from, $lte: to } });
        res.send({ response });
    } catch (error) {
        res.status(500).send( {  error } );
    }
});

router.post("/postMultipleRooms", async (req, res) => {
    const { data } = req.body;

    for (rm of data) {
        try {
            const date = new Date(rm.date * 1000); // timestamp to date
            const id = await getIdOfExistingRoomInfo(date);
            if (id != null) {
                await RoomsInfo.updateOne({ _id: id }, { $set: { ...rm, date } });
                continue;
            }
            const roomInfo = new RoomsInfo({ ...rm, date });
            await roomInfo.save();
        } catch (error) {
            console.log(error);
            res.status(500).send( {  error } );
        }
    }
    res.status(200).send({ message: "succsess" });
});

/* router.delete("/deleteAll", async (req, res) => {
    try {
        const response = await RoomsInfo.deleteMany();
        res.status(200).send({ message: "succsess" });
    } catch (err) {
        res.status(500).send({ message: err });
    }
}); */

async function getIdOfExistingRoomInfo(date) {
    try {
        const response = await RoomsInfo.findOne({ date });
        return response._id ? response._id : null;
    } catch (err) {
        return null;
    }
}

module.exports = router;

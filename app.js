const express = require("express");
const app = express();
require("./db/connection");
const auth = require("./middleware/auth");
const roomsRoute = require("./routes/rooms");

const port = process.env.PORT || 3000;

app.use(express.json());
app.use(auth);
app.use("/rooms", roomsRoute);

app.get("/", (req, res) => {
    res.send("Home");
});

app.listen(port);
